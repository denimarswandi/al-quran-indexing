import pandas as pd
from db import commit_sql

def save(nama_surat, nomor_surat):
  sql = """insert into quran values(%s, %s, %s)"""
  params = [0, nama_surat, nomor_surat]
  commit_sql(sql, params)

if __name__=='__main__':
  data = pd.read_excel(r'quran.xlsx', sheet_name='Sheet1')
  quran  = data.to_dict(orient='record')
  name_ = ''
  no_quran = 0
  for q in quran:
    name_quran = q['أسم السورة']
    nomor_surat = q['رقم السورة']
    if name_ != name_quran:
      if no_quran <=5:
        print(name_quran)
      name_ = name_quran
      save(name_, nomor_surat)
