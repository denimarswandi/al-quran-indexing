import pandas as pd
from db import commit_sql

#convert str to float
def convert_float(str_k):
  new_fl = ''
  for i in range(len(str_k)):
    if str_k[i]==',':
      new_fl = new_fl+'.'
    else:
      new_fl = new_fl+str(str_k[i])
  return float(new_fl)

def i_quran(nama_surat, nomor_surat, nomor_ayat, halaman, index_baris):
  sql = """insert into index_quran values(%s,%s, %s, %s, %s,%s)"""
  params = [0,nama_surat, nomor_surat, nomor_ayat, halaman, index_baris]
  commit_sql(sql, params)
if __name__=='__main__':
  data = pd.read_excel(r'quran.xlsx', sheet_name='Sheet1')
  quran  = data.to_dict(orient='recordrs')
  #print(quran)
  loop_i = 0
  for q in quran:
    nama_surat = q['أسم السورة']
    nomor_surat = q['رقم السورة']
    nomor_ayat = q['رقم الآية']
    halaman = q['halaman']
    index_baris = q['index baris']
    f_index_baris = 0.0
   
    if(isinstance(index_baris, str)):
      #print(convert_float(index_baris))
      f_index_baris = convert_float(index_baris)
    else:
      f_index_baris = index_baris
    i_quran(nama_surat, nomor_surat, nomor_ayat, halaman, f_index_baris)
    loop_i +=1

print(loop_i)


#print(quran)