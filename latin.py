import pandas as pd
from db import commit_sql

def u_quran(nama_surat, nomor_surat):
  sql ="""update quran set latin=%s where nomor_surat=%s"""
  params = [nama_surat, nomor_surat]
  commit_sql(sql, params)


if __name__=='__main__':
  data = pd.read_excel(r'latin.xlsx', sheet_name='Sheet1')
  quran = data.to_dict(orient='records')
  for q in quran:
    nomor_surat =q['no']
    nama_surat = q['ayat']
    u_quran(nama_surat, nomor_surat)