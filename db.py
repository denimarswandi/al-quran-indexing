import pymysql


def gcon():
  db = pymysql.connect('localhost', 'root','root','spp')
  return db

def commit_sql(sql, params):
  con = gcon()
  cur = con.cursor()
  try:
    cur.execute(sql, params)
    con.commit()
  except Exception as e:
    print(e)
  finally:
    con.close()