-- MySQL dump 10.13  Distrib 5.7.20, for osx10.13 (x86_64)
--
-- Host: localhost    Database: spp
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `quran`
--

DROP TABLE IF EXISTS `quran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_surat` varchar(255) DEFAULT NULL,
  `nomor_surat` int(11) DEFAULT NULL,
  `latin` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quran`
--

LOCK TABLES `quran` WRITE;
/*!40000 ALTER TABLE `quran` DISABLE KEYS */;
INSERT INTO `quran` VALUES (1,'سورة الفاتحة',1,'Al-Fatihah'),(2,'سورة البقرة ',2,'Al-Baqarah'),(3,'سورة آل عمران',3,'Ali \'Imran'),(4,'سورة النساء',4,'An-Nisa\''),(5,'سورة المائدة ',5,'Al-Ma\'idah'),(6,'سورة الأنعام',6,'Al-An\'am'),(7,'سورة الأعراف',7,'Al-A\'raf'),(8,'سورة الأنفال',8,'Al-Anfal'),(9,'سورة التوبة ',9,'At-Taubah'),(10,'سورة يونس',10,'Yunus'),(11,'سورة هود',11,'Hud'),(12,'سورة يوسف',12,'Yusuh'),(13,'سورة الرعد',13,'Ar-Ra\'d'),(14,'سورة إبراهيم ',14,'Ibrahim'),(15,'سورة الحجر',15,'Al-Hijr'),(16,'سورة النحل',16,'An-Nahl'),(17,'سورة الإسراء',17,'Al-Isra\''),(18,'سورة الكهف',18,'Al-Kahf'),(19,'سورة مريم ',19,'Maryam'),(20,'سورة طه',20,'Ta Ha'),(21,'سورة الأنبياء',21,'Al-Anbiya'),(22,'سورة الحج ',22,'Al-Hajj'),(23,'سورة المؤمنون ',23,'Al-Mu\'minun'),(24,'سورة النور',24,'An-Nur'),(25,'سورة الفرقان',25,'Al-Furqan'),(26,'سورة الشعراء',26,'Asy-Syu\'ara\''),(27,'سورة النمل ',27,'An-Naml'),(28,'سورة القصص',28,'Al-Qasas'),(29,'سورة العنكبوت',29,'Al-\'Ankabut'),(30,'سورة الروم',30,'Ar-Rum'),(31,'سورة لقمان ',31,'Luqman'),(32,'سورة السجدة',32,'As-Sajdah'),(33,'سورة الأحزاب',33,'Al-Ahzab'),(34,'سورة سبأ ',34,'Saba'),(35,'سورة فاطر',35,'Fatir'),(36,'سورة يس',36,'Ya Sin'),(37,'سورة الصافات',37,'As-Saffat'),(38,'سورة ص',38,'Sad'),(39,'سورة الزمر',39,'Az-Zumar'),(40,'سورة غافر',40,'Al-Mu\'min'),(41,'سورة فصلت',41,'Fussilat'),(42,'سورة الشورى ',42,'Asy-Syura'),(43,'سورة الزخرف',43,'Az-Zukhruf'),(44,'سورة الدخان',44,'Ad-Dukhan'),(45,'سورة الجاثية',45,'Al-Jasiyah'),(46,'سورة الأحقاف',46,'Al-Ahqaf'),(47,'سورة محمد ',47,'Muhammad'),(48,'سورة الفتح',48,'Al-Fath'),(49,'سورة الحجرات',49,'Hujurat'),(50,'سورة ق ',50,'Qaf'),(51,'سورة الذاريات',51,'Az-Zariyat'),(52,'سورة الطور',52,'At-Tur'),(53,'سورة النجم',53,'An-Najm'),(54,'سورة القمر ',54,'Al-Qamar'),(55,'سورة الرحمن ',55,'Ar-Rahman'),(56,'سورة الواقعة ',56,'Al-Waqi\'ah'),(57,'سورة الحديد',57,'Al-Hadid'),(58,'سورة المجادلة',58,'Al-Mujadilah'),(59,'سورة الحشر',59,'Al-Hasyr'),(60,'سورة الممتحنة ',60,'Al-Mumtahanah'),(61,'سورة الصف',61,'As-Saff'),(62,'سورة الجمعة ',62,'Al-Jumu\'ah'),(63,'سورة المنافقون ',63,'Al-Munafiqun'),(64,'سورة التغابن',64,'At-Tagabun'),(65,'سورة الطلاق',65,'At-Talaq'),(66,'سورة التحريم ',66,'At-Tahrim'),(67,'سورة الملك ',67,'Al-Mulk'),(68,'سورة القلم ',68,'Al-Qalam'),(69,'سورة الحاقة',69,'Al-Haqqah'),(70,'سورة المعارج',70,'Al-Ma\'arij'),(71,'سورة نوح ',71,'Nuh'),(72,'سورة الجن ',72,'Al-Jinn'),(73,'سورة المزمل',73,'Al-Muzammil'),(74,'سورة المدثر',74,'Al-Muddassir'),(75,'سورة القيامة ',75,'Al-Qiyamah'),(76,'سورة الإنسان ',76,'Al-Insan'),(77,'سورة المرسلات',77,'Al-Mursalat'),(78,'سورة النبأ ',78,'An-Naba\''),(79,'سورة النازعات',79,'An-Nazi\'at'),(80,'سورة عبس',80,'Abasa'),(81,'سورة التكوير',81,'At-Takwir'),(82,'سورة الإنفطار',82,'Al-Infitar'),(83,'سورة المطففين ',83,'Al-Mutaffifin'),(84,'سورة الانشقاق',84,'Al-Insyiqaq'),(85,'سورة البروج',85,'Al-Buruj'),(86,'سورة الطارق',86,'At-Tariq'),(87,'سورة الأعلى ',87,'Al-A\'ala'),(88,'سورة الغاشية ',88,'Al-Gasyiyah'),(89,'سورة الفجر',89,'Al-Fajr'),(90,'سورة البلد',90,'Al-Balad'),(91,'سورة الشمس',91,'Asy-Syams'),(92,'سورة الليل',92,'Al-Lail'),(93,'سورة الضحى',93,'A-Duha'),(94,'سورة الشرح',94,'Al-Insyirah'),(95,'سورة التين ',95,'At-Tin'),(96,'سورة العلق',96,'Al-\'Alaq'),(97,'سورة القدر',97,'Al-Qadr'),(98,'سورة البينة ',98,'Al-Bayyinah'),(99,'سورة الزلزلة ',99,'Az-Zalzalah'),(100,'سورة العاديات ',100,'Al-\'Adiyat'),(101,'سورة القارعة ',101,'Al-Qari\'ah'),(102,'سورة التكاثر',102,'At-Takasur'),(103,'سورة العصر',103,'Al-\'Asr'),(104,'سورة الهمزة ',104,'Al-Humazah'),(105,'سورة الفيل',105,'Al-Fil'),(106,'سورة قريش',106,'Quraisy'),(107,'سورة الماعون ',107,'Al-Ma\'un'),(108,'سورة الكوثر',108,'Al-Kausar'),(109,'سورة الكافرون ',109,'Al-Kafirun'),(110,'سورة النصر',110,'An-Nasr'),(111,'سورة المسد',111,'Al-Lahab'),(112,'سورة الإخلاص',112,'Al-Ikhlas'),(113,'سورة الفلق',113,'Al-Falaq'),(114,'سورة الناس',114,'An-Nas');
/*!40000 ALTER TABLE `quran` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-30 10:48:48
